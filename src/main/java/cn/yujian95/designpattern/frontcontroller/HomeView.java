package cn.yujian95.designpattern.frontcontroller;

/**
 * @author yujian95 clj9509@163.com
 * @date 3/8/2021
 */

public class HomeView {
    public void show(){
        System.out.println("Displaying Home Page");
    }
}
