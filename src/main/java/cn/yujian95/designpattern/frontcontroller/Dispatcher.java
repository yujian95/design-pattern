package cn.yujian95.designpattern.frontcontroller;

/**
 * @author yujian95 clj9509@163.com
 * @date 3/8/2021
 */

public class Dispatcher {
    private StudentView studentView;
    private HomeView homeView;

    public Dispatcher(){
        studentView = new StudentView();
        homeView = new HomeView();
    }

    public void dispatch(String request){
        if(request.equalsIgnoreCase("STUDENT")){
            studentView.show();
        }else{
            homeView.show();
        }
    }
}
