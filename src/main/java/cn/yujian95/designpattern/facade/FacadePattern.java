package cn.yujian95.designpattern.facade;

/**
 * @author yujian95 clj9509@163.com
 * @date 1/8/2021
 */

public class FacadePattern {
    public static void main(String[] args) {
        ShapeMaker shapeMaker = new ShapeMaker();

        shapeMaker.drawCircle();
        shapeMaker.drawRectangle();
        shapeMaker.drawSquare();
    }
}
