package cn.yujian95.designpattern.command;

/**
 * @author yujian95 clj9509@163.com
 * @date 3/14/2021
 */

public class BuyStock implements Order {
    private Stock abcStock;

    public BuyStock(Stock abcStock) {
        this.abcStock = abcStock;
    }

    @Override
    public void execute() {
        abcStock.buy();
    }
}
