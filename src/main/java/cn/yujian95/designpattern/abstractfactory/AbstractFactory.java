package cn.yujian95.designpattern.abstractfactory;

/**
 * @author yujian95 clj9509@163.com
 * @date 12/16/2020
 */

public abstract class AbstractFactory {
    public abstract Color getColor(String color);

    public abstract Shape getShape(String shape);
}
