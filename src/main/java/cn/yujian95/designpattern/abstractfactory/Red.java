package cn.yujian95.designpattern.abstractfactory;

/**
 * @author yujian95 clj9509@163.com
 * @date 12/16/2020
 */

public class Red implements Color {
    public void fill() {
        System.out.println("Red::fill()!");
    }
}
