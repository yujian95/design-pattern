package cn.yujian95.designpattern.template;

/**
 * @author yujian95 clj9509@163.com
 * @date 12/28/2020
 */

public class Football extends Game {
    @Override
    void endPlay() {
        System.out.println("Football Game Finished!");
    }

    @Override
    void initialize() {
        System.out.println("Football Game Initialized! Start playing.");
    }

    @Override
    void startPlay() {
        System.out.println("Football Game Started. Enjoy the game!");
    }
}
