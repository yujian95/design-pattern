package cn.yujian95.designpattern.template;

/**
 * @author yujian95 clj9509@163.com
 * @date 12/28/2020
 */

public abstract class Game {
    abstract void initialize();

    abstract void startPlay();

    abstract void endPlay();

    // 模板
    public final void play() {
        initialize();
        startPlay();
        endPlay();
    }
}
