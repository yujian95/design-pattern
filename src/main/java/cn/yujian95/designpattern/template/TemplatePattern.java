package cn.yujian95.designpattern.template;

/**
 * @author yujian95 clj9509@163.com
 * @date 12/28/2020
 */

public class TemplatePattern {
    public static void main(String[] args) {
        Game game = new Cricket();
        game.play();
        System.out.println();
        game = new Football();
        game.play();
    }
}
