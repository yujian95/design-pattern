package cn.yujian95.designpattern.builder;

/**
 * @author yujian95 clj9509@163.com
 * @date 1/8/2021
 */

public class VegBurger extends Burger {
    @Override
    public String name() {
        return "Veg Burger";
    }

    @Override
    public float price() {
        return 25.0f;
    }
}
