package cn.yujian95.designpattern.builder;

/**
 * @author yujian95 clj9509@163.com
 * @date 1/8/2021
 */

public class ChickenBurger extends Burger {
    @Override
    public String name() {
        return "Chicken Burger";
    }

    @Override
    public float price() {
        return 50.0f;
    }
}
