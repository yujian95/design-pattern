package cn.yujian95.designpattern.builder;

/**
 * @author yujian95 clj9509@163.com
 * @date 1/8/2021
 */

public class Coke extends ColdDrink {
    @Override
    public String name() {
        return "Coke";
    }

    @Override
    public float price() {
        return 35.0f;
    }
}
