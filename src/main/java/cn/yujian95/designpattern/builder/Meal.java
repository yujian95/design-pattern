package cn.yujian95.designpattern.builder;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yujian95 clj9509@163.com
 * @date 1/8/2021
 */

public class Meal {
    private List<Item> itemList = new ArrayList<Item>();

    public void addItem(Item item) {
        itemList.add(item);
    }

    public float getCost() {
        float cost = 0.0f;
        for (Item item : itemList) {
            cost += item.price();
        }
        return cost;
    }

    public void showItemList() {
        for (Item item : itemList) {
            System.out.print("Item : " + item.name());
            System.out.print(", Packing : " + item.packing().pack());
            System.out.println(", Price : " + item.price());
        }
    }
}
