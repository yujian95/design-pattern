package cn.yujian95.designpattern.builder;

/**
 * @author yujian95 clj9509@163.com
 * @date 1/8/2021
 */

public class Wrapper implements Packing {
    @Override
    public String pack() {
        return "Wrapper";
    }
}
