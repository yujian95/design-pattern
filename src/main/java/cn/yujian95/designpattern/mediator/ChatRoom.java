package cn.yujian95.designpattern.mediator;

import java.util.Date;

/**
 * @author yujian95 clj9509@163.com
 * @date 3/14/2021
 */

public class ChatRoom {
    public static void showMessage(User user, String message) {
        System.out.println(new Date().toString() + " [" + user.getName() + "] : " + message);
    }
}
