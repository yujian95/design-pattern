package cn.yujian95.designpattern.flyweight;

/**
 * @author yujian95 clj9509@163.com
 * @date 1/9/2021
 */
public interface Shape {
    void draw();
}
