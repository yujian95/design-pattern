package cn.yujian95.designpattern.chainresponsibility;

/**
 * @author yujian95 clj9509@163.com
 * @date 3/10/2021
 */

public class ErrorLogger extends AbstractLogger {

    public ErrorLogger(int level) {
        this.level = level;
    }

    @Override
    protected void write(String message) {
        System.out.println("error console: " + message);
    }
}
