package cn.yujian95.designpattern.proxy;

/**
 * @author yujian95 clj9509@163.com
 * @date 12/24/2020
 */

public interface Image {
    void display();
}
