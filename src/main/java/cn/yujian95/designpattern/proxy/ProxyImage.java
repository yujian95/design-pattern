package cn.yujian95.designpattern.proxy;

/**
 * @author yujian95 clj9509@163.com
 * @date 12/24/2020
 */

public class ProxyImage implements Image {
    private RealImage realImage;
    private String fileName;

    public ProxyImage(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void display() {
        if (realImage == null) {
            realImage = new RealImage(fileName);
        }
        realImage.display();
    }
}
