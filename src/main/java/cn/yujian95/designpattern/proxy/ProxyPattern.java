package cn.yujian95.designpattern.proxy;

/**
 * @author yujian95 clj9509@163.com
 * @date 12/24/2020
 */

public class ProxyPattern {

    public static void main(String[] args) {
        Image image = new ProxyImage("test_10mb.jpg");

        // 图像将从磁盘加载
        image.display();
        System.out.println("");
        // 图像不需要从磁盘加载
        image.display();
    }
}
