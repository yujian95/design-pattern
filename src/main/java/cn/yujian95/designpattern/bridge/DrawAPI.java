package cn.yujian95.designpattern.bridge;

/**
 * @author yujian95 clj9509@163.com
 * @date 12/24/2020
 */

public interface DrawAPI {
    void drawCircle(int radius, int x, int y);
}
