package cn.yujian95.designpattern.bridge;

/**
 * @author yujian95 clj9509@163.com
 * @date 12/24/2020
 */

public class BridgePattern {
    public static void main(String[] args) {
        Shape redCircle = new Circle(100, 100, 10, new RedCircle());
        Shape greenCircle = new Circle(100, 100, 10, new GreenCircle());

        redCircle.draw();
        greenCircle.draw();
    }
}
