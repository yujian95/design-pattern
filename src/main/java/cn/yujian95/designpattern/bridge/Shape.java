package cn.yujian95.designpattern.bridge;

/**
 * @author yujian95 clj9509@163.com
 * @date 12/24/2020
 */

public abstract class Shape {

    protected DrawAPI drawAPI;

    protected Shape(DrawAPI drawAPI) {
        this.drawAPI = drawAPI;
    }

    public abstract void draw();
}
