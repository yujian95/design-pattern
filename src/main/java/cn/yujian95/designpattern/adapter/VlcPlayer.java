package cn.yujian95.designpattern.adapter;

/**
 * @author yujian95 clj9509@163.com
 * @date 1/8/2021
 */

public class VlcPlayer implements AdvancedMediaPlayer {
    @Override
    public void playVlc(String fileName) {
        System.out.println("Playing vlc file. Name: "+ fileName);
    }

    @Override
    public void playMp4(String fileName) {
        //什么也不做
    }
}
