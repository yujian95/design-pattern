package cn.yujian95.designpattern.adapter;

/**
 * @author yujian95 clj9509@163.com
 * @date 1/8/2021
 */
public interface MediaPlayer {
    void play(String audioType,String fileName);
}
