package cn.yujian95.designpattern.adapter;

/**
 * @author yujian95 clj9509@163.com
 * @date 1/8/2021
 */

public class Mp4Player implements AdvancedMediaPlayer {
    @Override
    public void playVlc(String fileName) {
        //什么也不做
    }

    @Override
    public void playMp4(String fileName) {
        System.out.println("Playing mp4 file. Name: "+ fileName);
    }
}
