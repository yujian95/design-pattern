package cn.yujian95.designpattern.adapter;

/**
 * @author yujian95 clj9509@163.com
 * @date 1/8/2021
 */
public interface AdvancedMediaPlayer {
    void playVlc(String fileName);

    void playMp4(String fileName);
}
