package cn.yujian95.designpattern.visitor;

public interface ComputerPart {
   public void accept(ComputerPartVisitor computerPartVisitor);
}
