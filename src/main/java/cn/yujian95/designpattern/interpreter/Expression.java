package cn.yujian95.designpattern.interpreter;

/**
 * @author yujian95 clj9509@163.com
 * @date 3/14/2021
 */

public interface Expression {
    boolean interpret(String context);
}
