package cn.yujian95.designpattern.prototype;

/**
 * @author yujian95 clj9509@163.com
 * @date 12/24/2020
 */

public class Circle extends Shape {

    public Circle() {
        type = "Circle";
    }

    @Override
    public void draw() {
        System.out.println("Inside Circle::draw() method.");
    }
}
