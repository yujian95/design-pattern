package cn.yujian95.designpattern.decorator;

/**
 * @author yujian95 clj9509@163.com
 * @date 1/8/2021
 */

public abstract class ShapeDecorator implements Shape {
    protected Shape decoratedShape;

    public ShapeDecorator(Shape decoratedShape) {
        this.decoratedShape = decoratedShape;
    }

    @Override
    public void draw() {
        decoratedShape.draw();
    }
}
