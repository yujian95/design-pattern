package cn.yujian95.designpattern.decorator;

/**
 * @author yujian95 clj9509@163.com
 * @date 1/8/2021
 */
public interface Shape {
    void draw();
}
