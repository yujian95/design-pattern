package cn.yujian95.designpattern.decorator;

/**
 * @author yujian95 clj9509@163.com
 * @date 1/8/2021
 */

public class Rectangle implements Shape {
    @Override
    public void draw() {
        System.out.println("Shape: Rectangle");
    }
}
