package cn.yujian95.designpattern.factory;

/**
 * @author yujian95 clj9509@163.com
 * @date 12/16/2020
 */
public interface Shape {
    void draw();
}
