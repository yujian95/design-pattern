package cn.yujian95.designpattern.observer;

/**
 * @author yujian95 clj9509@163.com
 * @date 12/28/2020
 */

public abstract class Observer {
    protected Subject subject;

    public abstract void update();
}
