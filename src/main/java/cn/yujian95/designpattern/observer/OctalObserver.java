package cn.yujian95.designpattern.observer;

/**
 * @author yujian95 clj9509@163.com
 * @date 12/28/2020
 */

public class OctalObserver extends Observer {

    public OctalObserver(Subject subject) {
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println("Octal String: " + Integer.toOctalString(subject.getState()));
    }
}
