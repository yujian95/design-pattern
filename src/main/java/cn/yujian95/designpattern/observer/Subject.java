package cn.yujian95.designpattern.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yujian95 clj9509@163.com
 * @date 12/28/2020
 */

public class Subject {
    private List<Observer> observerList = new ArrayList<Observer>();
    private int state;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public void attach(Observer observer) {
        observerList.add(observer);
    }

    public void notifyAllObserverList() {
        for (Observer observer : observerList) {
            observer.update();
        }
    }
}
