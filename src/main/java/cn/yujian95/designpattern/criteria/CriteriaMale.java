package cn.yujian95.designpattern.criteria;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yujian95 clj9509@163.com
 * @date 1/8/2021
 */

public class CriteriaMale implements Criteria {
    @Override
    public List<Person> meetCriteria(List<Person> personList) {
        List<Person> malePersonList = new ArrayList<Person>();

        for (Person person : personList) {
            if ("MALE".equalsIgnoreCase(person.getGender())) {
                malePersonList.add(person);
            }
        }
        return malePersonList;
    }
}
