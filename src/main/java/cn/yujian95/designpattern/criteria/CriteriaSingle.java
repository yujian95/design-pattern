package cn.yujian95.designpattern.criteria;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yujian95 clj9509@163.com
 * @date 1/8/2021
 */

public class CriteriaSingle implements Criteria {
    @Override
    public List<Person> meetCriteria(List<Person> personList) {
        List<Person> singlePersonList = new ArrayList<Person>();

        for (Person person : personList) {
            if ("SINGLE".equalsIgnoreCase(person.getMaritalStatus())) {
                singlePersonList.add(person);
            }
        }
        return singlePersonList;
    }
}
