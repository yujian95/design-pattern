package cn.yujian95.designpattern.interceptingfilter;

public class Target {
   public void execute(String request){
      System.out.println("Executing request: " + request);
   }
}