package cn.yujian95.designpattern.interceptingfilter;

public interface Filter {
   public void execute(String request);
}