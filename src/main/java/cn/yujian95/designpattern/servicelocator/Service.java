package cn.yujian95.designpattern.servicelocator;

public interface Service {
   public String getName();
   public void execute();
}