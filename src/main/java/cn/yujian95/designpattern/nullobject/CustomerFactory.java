package cn.yujian95.designpattern.nullobject;

/**
 * @author yujian95 clj9509@163.com
 * @date 3/8/2021
 */

public class CustomerFactory {
    public static final String[] names = {"Rob", "Joe", "Julie"};

    public static AbstractCustomer getCustomer(String name) {
        // 这里模拟获取数据库数据
        for (String s : names) {
            if (s.equalsIgnoreCase(name)) {
                return new RealCustomer(name);
            }
        }
        // 不存在时，返回一个空对象，而不是空。也可以返回一个默认值的对象
        return new NullCustomer();
    }
}
