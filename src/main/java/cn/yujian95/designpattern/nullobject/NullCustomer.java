package cn.yujian95.designpattern.nullobject;

/**
 * @author yujian95 clj9509@163.com
 * @date 3/8/2021
 */

public class NullCustomer extends AbstractCustomer {

    @Override
    public String getName() {
        return "Not Available in Customer Database!";
    }

    @Override
    public boolean isNil() {
        return true;
    }
}
