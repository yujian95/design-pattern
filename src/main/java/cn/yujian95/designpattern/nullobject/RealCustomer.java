package cn.yujian95.designpattern.nullobject;

/**
 * @author yujian95 clj9509@163.com
 * @date 3/8/2021
 */

public class RealCustomer extends AbstractCustomer {

    public RealCustomer(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isNil() {
        return false;
    }
}
