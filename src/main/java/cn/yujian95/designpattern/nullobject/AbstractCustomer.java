package cn.yujian95.designpattern.nullobject;

/**
 * @author yujian95 clj9509@163.com
 * @date 3/8/2021
 */

public abstract class AbstractCustomer {

    protected String name;

    public abstract String getName();

    public abstract boolean isNil();
}
