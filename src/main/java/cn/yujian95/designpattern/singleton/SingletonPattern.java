package cn.yujian95.designpattern.singleton;

/**
 * @author yujian95 clj9509@163.com
 * @date 12/24/2020
 */

public class SingletonPattern {

    public static void main(String[] args) {

        SingleObject object = SingleObject.getInstance();
        object.showMessage();

    }
}
