package cn.yujian95.designpattern.state;

/**
 * @author yujian95 clj9509@163.com
 * @date 3/14/2021
 */

public class Context {

    private State state;

    public Context() {
        state = null;
    }

    public void setState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }
}
