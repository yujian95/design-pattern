package cn.yujian95.designpattern.state;

/**
 * @author yujian95 clj9509@163.com
 * @date 3/14/2021
 */
public interface State {
    public void doAction(Context context);
}
