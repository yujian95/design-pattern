package cn.yujian95.designpattern.state;

/**
 * @author yujian95 clj9509@163.com
 * @date 3/14/2021
 */

public class StartState implements State {
    @Override
    public void doAction(Context context) {
        System.out.println("Player is in start state");
        context.setState(this);
    }

    @Override
    public String toString(){
        return "Start State";
    }
}
