package cn.yujian95.designpattern.strategy;

/**
 * @author yujian95 clj9509@163.com
 * @date 3/8/2021
 */
public interface Strategy {
    int doOperation(int num1, int num2);
}
