package cn.yujian95.designpattern.strategy;

/**
 * @author yujian95 clj9509@163.com
 * @date 3/8/2021
 */

public class Context {
    private Strategy strategy;

    public Context(Strategy strategy) {
        this.strategy = strategy;
    }

    public int executeStrategy(int num1, int num2) {
        return strategy.doOperation(num1, num2);
    }
}
