package cn.yujian95.designpattern.iterator;

/**
 * @author yujian95 clj9509@163.com
 * @date 3/14/2021
 */

public interface Iterator {
    boolean hasNext();

    Object next();
}
