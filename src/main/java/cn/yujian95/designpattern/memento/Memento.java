package cn.yujian95.designpattern.memento;

/**
 * @author yujian95 clj9509@163.com
 * @date 3/14/2021
 */

public class Memento {
    private String state;

    public Memento(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }
}
