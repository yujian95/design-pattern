package cn.yujian95.designpattern.memento;

/**
 * @author yujian95 clj9509@163.com
 * @date 3/14/2021
 */

public class Originator {
    private String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Memento saveStateToMemento() {
        return new Memento(state);
    }

    public void getStateFromMemento(Memento memento) {
        state = memento.getState();
    }
}
